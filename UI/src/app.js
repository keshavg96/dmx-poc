const keccak256 = require('keccak');
const mainDeployer = require('./assets/contracts/MainDeployer.json');


let account;

document.querySelector('form').addEventListener('submit', handleSubmitForm);

window.addEventListener('load', async () => {
  if (typeof window.ethereum !== 'undefined') {
    console.log("MetaMask is Available :) !");
  }
  // Modern DApp browsers
  if (window.ethereum) {
    window.web3 = new Web3(ethereum);
    // To prevent the page reloading when the MetaMask network changes
    ethereum.autoRefreshOnNetworkChange = false;
    // To Capture the account details from MetaMask
    const accounts = await ethereum.enable();
    account = accounts[0];
  }

  // Legacy DApp browsers
  else if (window.web3) {
    window.web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/b54a4e3197e54250836d6e5726bdc58f"));
  }
  // Non-DApp browsers
  else {
    console.log('Non-Ethereum browser detected. Please install MetaMask');
  }
});

function handleSubmitForm(e) {
  e.preventDefault();
  let ta = document.querySelector('textarea');
  if (ta.value != '') {
    parseVal(ta.value);
  }
  // ta.value = '';
}

function parseVal(inputString) {
  let hash = keccak256('keccak256').update(inputString).digest('hex');
  let mineObject = JSON.parse(inputString);

  d = document.getElementById('mineHash');
  d.innerHTML = hash;

  createMine(
    mineObject.projectDetails.name,
    mineObject.projectDetails.availableTokens,
    mineObject.mineDetails.name,
    hash
  );
}

async function createMine(symbol, tokenCap, name, hash) {
  var mainDeployerContract = new web3.eth.Contract(
    mainDeployer.abi,
    mainDeployer.networks[3].address
  );

  listenToEvent(mainDeployerContract);

  let transactId = await mainDeployerContract.methods.launchMine(
    name,
    symbol,
    hash,
    0,
    tokenCap
  ).send({
    from: account, gas: 5000000
  });

  console.log(transactId);
  d = document.getElementById('transactionAddr');
  d.innerHTML = transactId.transactionHash;
}

function listenToEvent(contract) {
  let options = {
    filter: { wallet: account }
  }
  contract.once('MineCreated', options, (err, event) => {
    if (event) {
      console.log(event);
      d = document.getElementById('mineAddr');
      d.innerHTML = event.returnValues.mine;
    }
  })
}