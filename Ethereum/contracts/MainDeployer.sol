pragma solidity 0.5.16;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./MineToken.sol";

contract MainDeployer is Ownable {
    using SafeMath for uint256;

    event MineCreated(address indexed wallet, address indexed mine);

    mapping(address => address) public _walletVsMine;

    constructor() public {}

    /**
     * @dev Launch Mine
     */
    function launchMine(
        string memory name,
        string memory symbol,
        string memory mineHash,
        uint8 decimals,
        uint256 cap
    ) public {
        MineToken mineToken = new MineToken(
            name,
            symbol,
            mineHash,
            decimals,
            cap,
            msg.sender
        );
        mineToken.mint(msg.sender, cap);

        _walletVsMine[msg.sender] = address(mineToken);
        emit MineCreated(msg.sender, address(mineToken));
    }
}
