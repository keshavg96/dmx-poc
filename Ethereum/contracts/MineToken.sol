pragma solidity 0.5.16;


import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Capped.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

//Only sales nominee will be allowed to mint tokens.
//So minter can be considered as salesNominee
contract MineToken is ERC20Capped, Ownable {
    using SafeMath for uint256;

    string private _name;
    string private _symbol;
    string private _hash;
    uint8 private _decimals;

    constructor(
        string memory name,
        string memory symbol,
        string memory mineHash,
        uint8 decimals,
        uint256 cap,
        address admin
    ) ERC20Capped(cap)
        public
    {
        _name = name;
        _symbol = symbol;
        _hash = mineHash;
        _decimals = decimals;
        transferOwnership(admin);
    }

    function name() external view returns(string memory) {
        return _name;
    }

    function symbol() external view returns(string memory) {
        return _symbol;
    }

    function hash() external view returns(string memory) {
        return _hash;
    }

    function decimals() external view returns(uint8) {
        return _decimals;
    }

    function _transfer(address sender, address recipient, uint256 amount) internal {
        super._transfer(sender, recipient, amount);
    }
}