#!/usr/bin/env bash

# Exit script as soon as a command fails.
set -o errexit

# Executes cleanup function at script exit.
trap cleanup EXIT

cleanup() {
  echo "cleanup1 $ganache_pid"
  # Kill the ganache instance that we started (if we started one and if it's still running).
  if [ -n "$ganache_pid" ] && ps -p $ganache_pid > /dev/null; then
    kill -9 $ganache_pid
  fi
}

ganache_port=8545

ganache_running() {
  nc -z localhost "$ganache_port"
}

start_ganache() {
  menomic_string="dog permit example repeat gloom defy teach pumpkin library remain scorpion skull"
  echo "Running Ganache"
  node_modules/.bin/ganache-cli --gasLimit 0xfffffffffff -m "$menomic_string" -e 1000 -a 100 -p $ganache_port > /dev/null &
  ganache_pid=$!
}

if ganache_running; then
  echo "Using existing ganache instance"

else
  echo "Starting our own ganache instance"
  start_ganache

  while :
  do
      if ganache_running
      then
        break
      fi
  done
  echo "Ganache up and Running"
fi

if [ "$SOLC_NIGHTLY" = true ]; then
  echo "Downloading solc nightly"
  wget -q https://raw.githubusercontent.com/ethereum/solc-bin/gh-pages/bin/soljson-nightly.js -O /tmp/soljson.js && find . -name soljson.js -exec cp /tmp/soljson.js {} \;
fi

node_modules/.bin/truffle migrate